Description
-----------
This project integrates PayPal into the Drupal Commerce payment and checkout systems.
More information about Bcash: https://www.bcash.com.br/

This module was built based on Commerce Paypal.

Requirements
------------
Drupal 7.x

Installation
------------
1. Enable this module and go to admin/commerce/config/payment-methods/manage/commerce_payment_commerce_bcash.
2. Enter the required credentials from your store on Bcash, store code, access code, store
email and Gateway Url which is currently https://www.bcash.com.br/checkout/pay/.
3. Configure in Bcash website the IPN gateway - http://[your-site-url]/commerce_bcash/ipn

Maintainer: utneon
Developed and sponsored by GT Informática - http://www.gtinformatica.pt

